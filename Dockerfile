FROM nginx:latest

COPY ./index2.html /usr/share/nginx/html/

RUN echo > /usr/share/nginx/html/index.html

RUN cat /usr/share/nginx/html/index2.html > /usr/share/nginx/html/index.html && rm -rf /usr/share/nginx/html/index2.html

RUN apt-get update && apt-get install -y curl

EXPOSE 80
